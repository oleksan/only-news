package com.oleksan.onlynews.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class OnlyNewsApp: Application()