package com.oleksan.onlynews.presentation.screens.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.oleksan.onlynews.domain.repository.OnlyNewsRepository
import com.oleksan.onlynews.domain.model.Article
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class SearchViewModel @Inject constructor(private val repository: OnlyNewsRepository) :
    ViewModel() {

    private val _searchNewsLiveData: MutableLiveData<List<Article>> = MutableLiveData()
    val searchNewsLiveData: LiveData<List<Article>>
        get() = _searchNewsLiveData

    private val searchNewsPage = 1

    init {
        getSearchNews("")
    }

    fun getSearchNews(query: String) =
        viewModelScope.launch {
            //  searchNewsLiveData.postValue(Resource.Loading())
            val response = repository.getSearchNews(query = query, pageNumber = searchNewsPage)
            if (response.isSuccessful) {
                response.body().let { res ->
                    _searchNewsLiveData.postValue(res?.articles)
                }
            } else {
                //  searchNewsLiveData.postValue(Resource.Error(message = response.message()))
            }
        }

}
