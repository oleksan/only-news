package com.oleksan.onlynews.data.api

import com.oleksan.onlynews.domain.model.NewsResponse
import com.oleksan.onlynews.utils.Constants.Companion.API_KEY
import com.oleksan.onlynews.utils.Constants.Companion.COUNTRY_CODE_UA
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface OnlyNewsService {

    @GET("/v2/everything")
    suspend fun getEverything(
        @Query("q") query: String,
        @Query("page") page: Int = 1,
        @Query("apiKey") apiKey: String = API_KEY
    ): Response<NewsResponse>

    @GET("/v2/top-headlines")
    suspend fun getHeadLines(
        @Query("country") countryCode: String = COUNTRY_CODE_UA,
        @Query("page") page: Int = 1,
        @Query("apiKey") apiKey: String = API_KEY
    ): Response<NewsResponse>

}