package com.oleksan.onlynews.domain.usecases

import com.oleksan.onlynews.domain.repository.OnlyNewsRepository
import javax.inject.Inject

class GetFavoriteArticlesUseCase @Inject constructor(private val onlyNewsRepository: OnlyNewsRepository) {
    operator fun invoke() = onlyNewsRepository.getFavoriteArticles()
}