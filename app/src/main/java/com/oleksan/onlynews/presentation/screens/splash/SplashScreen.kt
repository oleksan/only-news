package com.oleksan.onlynews.presentation.screens.splash

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.oleksan.onlynews.presentation.screens.main.MainViewModel
import com.oleksan.onlynews.ui.components.BottomNavItem
import com.oleksan.onlynews.ui.theme.Teal
import kotlinx.coroutines.delay

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun SplashScreen(navController: NavController, mainViewModel: MainViewModel) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Teal)
            .wrapContentSize(Alignment.Center)
    ) {
        Text(modifier = Modifier.align(Alignment.CenterHorizontally),
            text = "Only News",
            fontSize = 72.sp,
            fontWeight = FontWeight.Bold,
            color = Color.White
        )
    }

    LaunchedEffect(key1 = true) {
        mainViewModel.getNews()
        delay(2000)
        navController.navigate(BottomNavItem.HOME.route)
    }
}