package com.oleksan.onlynews.presentation.screens.search

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.oleksan.onlynews.R
import com.oleksan.onlynews.ui.components.NewsItem
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Composable
fun SearchScreen(navController: NavController, searchViewModel: SearchViewModel) {

    var search by rememberSaveable { mutableStateOf("") }
    val searchNews = searchViewModel.searchNewsLiveData.observeAsState(listOf()).value

    Column(
        modifier = Modifier
            .fillMaxSize()
          //  .background(colorResource(id = R.color.white))
    ) {
        Text(
            text = "Search",
            fontWeight = FontWeight.Bold,
            color = Color.Black,
            modifier = Modifier
                .padding(top = 18.dp)
                .padding(horizontal = 15.dp),
            textAlign = TextAlign.Center,
            fontSize = 20.sp
        )
        TextField(modifier = Modifier
            .fillMaxWidth()
            .padding(top = 19.dp)
            .padding(horizontal = 19.dp)
            .padding(vertical = 24.dp),
            value = search, onValueChange = {
                search = it

                MainScope().launch {
                    delay(500L)

                    if (it.isNotEmpty()) {
                        searchViewModel.getSearchNews(query = it)
                    }

                }
            },
            label = { Text(text = "Search...") })

        LazyColumn(
            modifier = Modifier
                .padding(20.dp)
        ) {
            items(searchNews) { item ->
                NewsItem(item = item, navController = navController)
            }
        }
    }
}