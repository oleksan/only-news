package com.oleksan.onlynews.presentation.screens.favorite

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.oleksan.onlynews.ui.theme.LightGrey
import com.oleksan.onlynews.ui.theme.OnlyNewsTheme

@Composable
fun FavoriteScreen() {

    Column(
        modifier = Modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(
            modifier = Modifier
                .wrapContentSize()
                .padding(top = 220.dp),
            contentAlignment = Alignment.Center
        )
        {
            Icon(
                imageVector = Icons.Default.Favorite,
                contentDescription = "Favorite",
                modifier = Modifier
                    .size(100.dp),
                tint = LightGrey
            )
        }
        Text(
            text = "No favorites yet",
            modifier = Modifier
                .padding(top = 50.dp)
                .padding(horizontal = 15.dp),
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center
        )

        Text(
            text = "Make sure you have\nadded event's in this section",
            modifier = Modifier
                .padding(top = 10.dp)
                .padding(horizontal = 15.dp),
            fontSize = 16.sp,
            textAlign = TextAlign.Center
        )
        Button(
            onClick = { /*TODO*/ },
            modifier = Modifier.padding(top = 30.dp),
        ) {
            Text(text = "ADD FAVORITES")
        }

    }
}

@Composable
@Preview(showBackground = true)
fun FavoriteScreenPreview() {
    OnlyNewsTheme {
        FavoriteScreen()
    }
}