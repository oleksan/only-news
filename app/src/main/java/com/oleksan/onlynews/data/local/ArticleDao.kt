package com.oleksan.onlynews.data.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.oleksan.onlynews.domain.model.Article

@Dao
interface ArticleDao {

    @Query("SELECT * FROM articles")
    fun getAllArticles(): LiveData<List<Article>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(article: Article)

    @Delete
    suspend fun delete(article: Article)
}