package com.oleksan.onlynews.ui.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Search
import androidx.compose.ui.graphics.vector.ImageVector

sealed class BottomNavItem(
    val route: String,
    val title: String,
    val icon: ImageVector
) {
    object HOME : BottomNavItem(
        route = "HOME",
        title = "HOME",
        icon = Icons.Default.Home
    )
    object SEARCH : BottomNavItem(
        route = "SEARCH",
        title = "SEARCH",
        icon = Icons.Default.Search
    )
    object FAVORITE : BottomNavItem(
        route = "FAVORITE",
        title = "FAVORITE",
        icon = Icons.Default.Favorite
    )
}