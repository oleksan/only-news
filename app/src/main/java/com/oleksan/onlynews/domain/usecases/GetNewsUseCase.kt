package com.oleksan.onlynews.domain.usecases

import com.oleksan.onlynews.domain.repository.OnlyNewsRepository
import javax.inject.Inject

class GetNewsUseCase @Inject constructor(private val onlyNewsRepository: OnlyNewsRepository) {
    suspend operator fun invoke(countryCode: String, pageNumber: Int) =
        onlyNewsRepository.getNews(countryCode = countryCode, pageNumber = pageNumber)
}