package com.oleksan.onlynews.presentation.screens.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.oleksan.onlynews.domain.repository.OnlyNewsRepository
import com.oleksan.onlynews.domain.model.Article
import com.oleksan.onlynews.utils.Constants.Companion.COUNTRY_CODE_UA
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class MainViewModel @Inject constructor(

    private val repository: OnlyNewsRepository
) : ViewModel() {

    private val _newsLiveData: MutableLiveData<List<Article>> = MutableLiveData()
    val newsLiveData: LiveData<List<Article>>
    get() = _newsLiveData
    private var newsPage = 1

   /* init {
        getNews()
    }*/

    /*private fun getNews(countryCode: String) =
        viewModelScope.launch {
          //  newsLiveData.postValue(Resource.Loading())
            val response = repository.getNews(countryCode = countryCode, pageNumber = newsPage)
            if (response.isSuccessful) {
                response.body().let { res ->
                    _newsLiveData.postValue(res?.articles)
                }
            } else {
          //      newsLiveData.postValue(Resource.Error(message = response.message()))
            }
        }*/

    fun getNews(){
        viewModelScope.launch {
            repository.getNews(COUNTRY_CODE_UA, pageNumber = newsPage).let{
                if (it.isSuccessful){
                    _newsLiveData.postValue(it.body()?.articles)
                } else {
                    Log.d("checkData", "Failed to load movies: ${it.errorBody()}")
                }
            }
        }
    }

}
