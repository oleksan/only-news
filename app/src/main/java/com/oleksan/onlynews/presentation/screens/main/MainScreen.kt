package com.oleksan.onlynews.presentation.screens.main

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.oleksan.onlynews.R
import com.oleksan.onlynews.ui.components.NewsItem


@Composable
fun MainScreen(navController: NavController, mainViewModel: MainViewModel) {

    val allArticles = mainViewModel.newsLiveData.observeAsState(listOf()).value

    Column(
        modifier = Modifier
            .fillMaxSize()
            //.background(colorResource(id = R.color.white))
    ) {
        Text(
            text = "Only News",
            fontWeight = FontWeight.Bold,
            color = Color.Black,
            modifier = Modifier.align(Alignment.CenterHorizontally),
            textAlign = TextAlign.Center,
            fontSize = 40.sp
        )
        Text(
            text = "Popular News",
            color = Color.Black,
            modifier = Modifier
                .padding(top = 42.dp)
                .padding(horizontal = 16.dp),
            textAlign = TextAlign.Center,
            fontSize = 16.sp
        )
        LazyColumn(
            modifier = Modifier
                .padding(8.dp)
        ) {
            items(allArticles) { item ->
                NewsItem(item = item, navController = navController)
            }
        }
    }
}

