package com.oleksan.onlynews.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.oleksan.onlynews.presentation.screens.favorite.FavoriteScreen
import com.oleksan.onlynews.presentation.screens.main.MainScreen
import com.oleksan.onlynews.presentation.screens.main.MainViewModel
import com.oleksan.onlynews.presentation.screens.search.SearchScreen
import com.oleksan.onlynews.presentation.screens.search.SearchViewModel
import com.oleksan.onlynews.presentation.screens.splash.SplashScreen
import com.oleksan.onlynews.ui.components.BottomNavItem

sealed class Screens(val route: String) {
    object Splash : Screens(route = "splash_screen")
}

@Composable
fun SetupNavHost(navController: NavHostController,
                 mainViewModel: MainViewModel,
                 searchViewModel: SearchViewModel
) {
    NavHost(
        navController = navController,
        startDestination = Screens.Splash.route
    ) {

        composable(route = Screens.Splash.route) {
            SplashScreen(navController = navController, mainViewModel = mainViewModel)
        }

        composable(route = BottomNavItem.HOME.route) {
            MainScreen(navController = navController, mainViewModel = mainViewModel)
        }
        composable(route = BottomNavItem.SEARCH.route) {
            SearchScreen(navController = navController, searchViewModel = searchViewModel)
        }
        composable(route = BottomNavItem.FAVORITE.route) {
            FavoriteScreen()
        }

    }
}