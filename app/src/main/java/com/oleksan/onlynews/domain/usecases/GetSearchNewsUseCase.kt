package com.oleksan.onlynews.domain.usecases

import com.oleksan.onlynews.domain.repository.OnlyNewsRepository
import javax.inject.Inject

class GetSearchNewsUseCase @Inject constructor(private val onlyNewsRepository: OnlyNewsRepository) {
    suspend operator fun invoke(query: String, pageNumber: Int) =
        onlyNewsRepository.getSearchNews(query = query, pageNumber = pageNumber)
}