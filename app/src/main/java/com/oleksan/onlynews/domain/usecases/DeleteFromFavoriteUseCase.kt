package com.oleksan.onlynews.domain.usecases

import com.oleksan.onlynews.domain.model.Article
import com.oleksan.onlynews.domain.repository.OnlyNewsRepository
import javax.inject.Inject

class DeleteFromFavoriteUseCase @Inject constructor(private val onlyNewsRepository: OnlyNewsRepository) {
    suspend operator fun invoke(article: Article) =
        onlyNewsRepository.deleteFromFavorite(article = article)
}