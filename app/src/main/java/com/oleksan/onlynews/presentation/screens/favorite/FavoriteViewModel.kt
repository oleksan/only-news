package com.oleksan.onlynews.presentation.screens.favorite

/*
@HiltViewModel
class FavoriteViewModel @Inject constructor(private val repository: OnlyNewsRepository) : ViewModel() {

    val savedLiveData: MutableLiveData<List<Article>> = MutableLiveData()
    private var newsPage = 1

    */
/*init {
        getSavedArticles()
    }*//*


    fun getSavedArticles() =
        repository.getFavoriteArticles()

    fun deleteFavoriteArticle(article: Article) = viewModelScope.launch(Dispatchers.IO) {
        repository.deleteFromFavorite(article = article)}


}*/
