package com.oleksan.onlynews

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.*
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.rememberNavController
import com.oleksan.onlynews.presentation.navigation.SetupNavHost
import com.oleksan.onlynews.presentation.screens.main.MainViewModel
import com.oleksan.onlynews.presentation.screens.search.SearchViewModel
import com.oleksan.onlynews.ui.components.BottomNavItem
import com.oleksan.onlynews.ui.components.BottomNavigationBar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            val mainViewModel = hiltViewModel<MainViewModel>()
            val searchViewModel = hiltViewModel<SearchViewModel>()

            Scaffold(
                bottomBar = {
                    BottomNavigationBar(
                        items = listOf(
                            BottomNavItem.HOME,
                            BottomNavItem.SEARCH,
                            BottomNavItem.FAVORITE
                        ),
                        navController = navController
                    )
                }
            ) {
                SetupNavHost(
                    navController = navController,
                    mainViewModel = mainViewModel,
                    searchViewModel = searchViewModel
                )
            }

        }
    }
}


