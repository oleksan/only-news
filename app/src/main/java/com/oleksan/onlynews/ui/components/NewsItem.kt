package com.oleksan.onlynews.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.oleksan.onlynews.domain.model.Article

@Composable
fun NewsItem(item: Article, navController: NavController) {

    Card(
        elevation = 4.dp,
        modifier = Modifier
            .padding(8.dp)
            .clickable {
                // navController.navigate(Screens.Details.route + "/${item.id}")
            }
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 16.dp)
        ) {

            AsyncImage(
                model = item.urlToImage,
                //  painter = rememberAsyncImagePainter(item.urlToImage),
                contentDescription = null,
                modifier = Modifier
                    .size(88.dp)
                    .padding(8.dp)
                    .clip(RoundedCornerShape(10.dp)),

                )

            Column {
                Text(
                    text = item.publishedAt,
                    fontSize = 12.sp
                )
                Text(
                    text = item.title,
                    fontSize = 12.sp,
                    fontWeight = FontWeight.Bold,
                    maxLines = 3
                )
            }
        }
    }
}