package com.oleksan.onlynews.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.oleksan.onlynews.domain.model.Article

@Database(entities = [Article::class], version = 1, exportSchema = true)
abstract class ArticleDatabase : RoomDatabase() {

    abstract fun getArticleDao(): ArticleDao

}