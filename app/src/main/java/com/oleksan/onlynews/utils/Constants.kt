package com.oleksan.onlynews.utils

class Constants {
    companion object {
        const val BASE_URL = "https://newsapi.org/"
        const val API_KEY = ""
        const val COUNTRY_CODE_UA = "ua"
    }
}