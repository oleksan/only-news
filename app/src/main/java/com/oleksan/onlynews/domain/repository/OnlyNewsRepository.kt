package com.oleksan.onlynews.domain.repository

import com.oleksan.onlynews.data.api.OnlyNewsService
import com.oleksan.onlynews.data.local.ArticleDao
import com.oleksan.onlynews.domain.model.Article
import javax.inject.Inject

class OnlyNewsRepository @Inject constructor(
    private val newsService: OnlyNewsService,
    private val articleDao: ArticleDao
) {
    suspend fun getNews(countryCode: String, pageNumber: Int) =
        newsService.getHeadLines(countryCode = countryCode, page = pageNumber)

    suspend fun getSearchNews(query: String, pageNumber: Int) =
        newsService.getEverything(query = query, page = pageNumber)

    fun getFavoriteArticles() = articleDao.getAllArticles()

    suspend fun addToFavorite(article: Article) = articleDao.upsert(article = article)

    suspend fun deleteFromFavorite(article: Article) = articleDao.delete(article = article)


}