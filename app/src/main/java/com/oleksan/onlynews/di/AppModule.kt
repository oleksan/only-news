package com.oleksan.onlynews.di

import android.content.Context
import androidx.room.Room
import com.oleksan.onlynews.data.api.OnlyNewsService
import com.oleksan.onlynews.data.local.ArticleDao
import com.oleksan.onlynews.data.local.ArticleDatabase
import com.oleksan.onlynews.utils.Constants.Companion.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun baseUrl() = BASE_URL

    @Provides
    fun login() = HttpLoggingInterceptor()
        .setLevel(HttpLoggingInterceptor.Level.BODY)

    @Provides
    fun okHttpClient() = OkHttpClient.Builder()
        .addInterceptor(login())
        .build()

    @Provides
    @Singleton
    fun provideRetrofit(baseUrl: String): OnlyNewsService =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient())
            .build()
            .create(OnlyNewsService::class.java)

    @Provides
    @Singleton
    fun provideArticleDatabase(@ApplicationContext context: Context) =
        Room.databaseBuilder(
            context,
            ArticleDatabase::class.java,
            "article_database"
        ).build()

    @Provides
    fun provideArticleDao(appDataBase: ArticleDatabase): ArticleDao {
        return appDataBase.getArticleDao()
    }
}