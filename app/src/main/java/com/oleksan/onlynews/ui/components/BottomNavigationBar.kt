package com.oleksan.onlynews.ui.components

import androidx.compose.foundation.layout.size
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.oleksan.onlynews.ui.theme.LightGrey
import com.oleksan.onlynews.ui.theme.Teal

@Composable
fun BottomNavigationBar(
    items: List<BottomNavItem>,
    navController: NavController
) {
    BottomNavigation(
        backgroundColor = LightGrey,
        elevation = 5.dp
    ) {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.destination?.route
        items.forEach { item ->
            BottomNavigationItem(
                icon = {
                    Icon(
                        item.icon,
                        modifier = Modifier.size(36.dp),
                        contentDescription = item.title
                    )
                },
                /*label = {
                    Text(
                        text = item.title,
                        fontSize = 9.sp
                    )
                },*/
                //  selectedContentColor = Color.Green,
                selectedContentColor = Teal,

                unselectedContentColor = Color.Black.copy(0.4f),
                //  alwaysShowLabel = true,
                selected = currentRoute == item.route,
                onClick = {
                    navController.navigate(item.route) {

                        navController.graph.startDestinationRoute?.let { screen_route ->
                            popUpTo(screen_route) {
                                saveState = true
                            }
                        }
                        launchSingleTop = true
                        restoreState = true
                    }
                }
            )
        }
    }
}
